# 1. Create an abstract class called Animal that has the following abstract methods:
# eat(food)
# make_sound()

# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties:
# Name
# Breed
# Age
# Methods:
# Getters
# Setters
# Implementation of abstract methods
# call()


from abc import ABC

class Animal(ABC):
    def eat(self, food):
        pass

    def make_sound(self):
        pass


class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaa!")

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

    def call(self):
        print(f"{self._name}, come on!")


class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

    def call(self):
        print(f"Here {self._name}!")


# test cases:
dog1 = Dog("Isis", "Dalmation", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

